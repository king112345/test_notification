<?php

namespace App\Http\Controllers;

use App\Notifications\PushDemo;
use App\User;
use Illuminate\Http\Request;
use Notification;
use Illuminate\Support\Facades\Auth;

class PushController extends Controller
{
    public function store(Request $request){
        $this->validate($request,[
            'endpoint'    => 'required',
            'keys.auth'   => 'required',
            'keys.p256dh' => 'required'
        ]);
        $endpoint = $request->endpoint;
        $token = $request->keys['auth'];
        $key = $request->keys['p256dh'];
        $user = Auth::user();
        $user->updatePushSubscription($endpoint, $key, $token);
        return response()->json(['success' => true],200);
    }

    public function push(){
        Notification::send(User::All(), new PushDemo);
        return redirect()->back();
    }
}
